const mongoose = require("mongoose");
const uri = process.env.MONGODB;

const db = async () => {
  await mongoose.connect(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
  console.log('Connect Success', uri);
};

module.exports = db
