const mongoose = require("mongoose");
const { Schema } = mongoose;

const studentSchema = new Schema({
  date: { type: Date, default: Date.now },
  hidden: Boolean,
  wallet: String,
  email: String,
  fullName: String,
  phoneNumber: String,
  payment: String,
});
const Student = mongoose.model("Student", studentSchema);
module.exports = Student;