require("dotenv").config();
const express = require("express");
const http = require("http");
const io = require("socket.io");
const bodyParser = require("body-parser");
const app = express();
const connectDB = require("./lib/mongoose.js")();

app.use(express.static("public"));
app.set("view engine", "ejs");
app.set("views", "./views");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(
  "/scripts",
  express.static(__dirname + "/node_modules/web3.js-browser/build")
);

// require("./controller/game")(app);

app.get("/", (req, res) => {
  res.render("master");
});

const server = http.createServer(app);
const serverSocket = io(server);

server.listen(3000, () => {
  console.log("Server start");
});
// Xuan hung update
// TODO: d12e21e12