const ethereumButton = document.querySelector(".enableEthereumButton");
let currentAccount;
let SM_ABI = [
  {
    inputs: [],
    stateMutability: "nonpayable",
    type: "constructor",
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: false,
        internalType: "uint256",
        name: "tongTien",
        type: "uint256",
      },
      {
        indexed: false,
        internalType: "address",
        name: "vi",
        type: "address",
      },
      {
        indexed: false,
        internalType: "uint256",
        name: "tien",
        type: "uint256",
      },
      {
        indexed: false,
        internalType: "string",
        name: "hoTen",
        type: "string",
      },
    ],
    name: "newTransaction",
    type: "event",
  },
  {
    inputs: [
      {
        internalType: "uint256",
        name: "",
        type: "uint256",
      },
    ],
    name: "arrHocSinh",
    outputs: [
      {
        internalType: "address",
        name: "_address",
        type: "address",
      },
      {
        internalType: "uint256",
        name: "_tien",
        type: "uint256",
      },
      {
        internalType: "string",
        name: "_hoTen",
        type: "string",
      },
    ],
    stateMutability: "view",
    type: "function",
  },
  {
    inputs: [
      {
        internalType: "uint256",
        name: "index",
        type: "uint256",
      },
    ],
    name: "getHocSinhByIdx",
    outputs: [
      {
        internalType: "address",
        name: "",
        type: "address",
      },
      {
        internalType: "uint256",
        name: "",
        type: "uint256",
      },
      {
        internalType: "string",
        name: "",
        type: "string",
      },
    ],
    stateMutability: "view",
    type: "function",
  },
  {
    inputs: [],
    name: "hocSinhCounter",
    outputs: [
      {
        internalType: "uint256",
        name: "",
        type: "uint256",
      },
    ],
    stateMutability: "view",
    type: "function",
  },
  {
    inputs: [
      {
        internalType: "string",
        name: "hoTen",
        type: "string",
      },
    ],
    name: "napTien",
    outputs: [],
    stateMutability: "payable",
    type: "function",
  },
  {
    inputs: [],
    name: "tongTien",
    outputs: [
      {
        internalType: "uint256",
        name: "",
        type: "uint256",
      },
    ],
    stateMutability: "view",
    type: "function",
  },
];
let SM_ADDRESS = "0x727582cDfcD4336658882f5A507A61eDcbA1d289";

$(document).ready(() => {
  if (typeof window.ethereum !== "undefined") {
    console.log("MetaMask is installed!");
    $("#install").hide(0);
    $("#info").show(1000);
    listenAccountChange();
    main();
  } else {
    console.log("MetaMask isn't installed!");
    $("#info").hide(0);
    $("#install").show(1000);
  }
});

ethereumButton.addEventListener("click", async () => {
  try {
    const accounts = await connectMetamask();
    updateCurrentAccount(accounts[0]);
  } catch (error) {}
});

async function connectMetamask() {
  const accounts = await ethereum.request({ method: "eth_requestAccounts" });
  return accounts;
}

async function listenAccountChange() {
  ethereum.on("accountsChanged", (accounts) => {
    updateCurrentAccount(accounts[0]);
  });
}
async function updateCurrentAccount(accounts) {
  currentAccount = accounts;
  $("#currentAccount").html(currentAccount);
}

async function main() {
  const web3 = new Web3(window.ethereum);
  window.ethereum.enable();
  const contractMM = web3.eth.Contract(SM_ABI, SM_ADDRESS);

  const provider = new Web3.providers.WebsocketProvider(
    "wss://rinkeby.infura.io/ws/v3/91e60a3054834489856634e79a3b1d58"
  );

  const web3_infura = new Web3(provider);
  const contractInfura = web3_infura.eth.Contract(SM_ABI, SM_ADDRESS);
  contractInfura.events.newTransaction(
    { filter: {}, fromBlock: "latest" },
    (err, data) => {
      if (err) {
        console.log(err);
      }
      console.log(data);
    }
  );

  $("#btn_send").click(async () => {
    const hoTen = $("#txt_hoten").val();
    const tien = $("#txt_tien").val();
    console.log(hoTen, tien);
    try {
      const result = contractMM.methods.napTien(hoTen).send({
        from: currentAccount,
        value: 1000000,
      });
      console.log("result", result);
    } catch (error) {
      console.log(error);
    }
  });
}
